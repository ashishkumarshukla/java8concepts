package com.practice.java8concepts;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

import static java.util.Comparator.comparing;

public class Practice {
	public static void main(String[] args) {
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");

		List<Transaction> transactions = Arrays.asList(new Transaction(brian, 2011, 300),
				new Transaction(raoul, 2012, 1000), new Transaction(raoul, 2011, 400),
				new Transaction(mario, 2012, 710), new Transaction(mario, 2012, 700), new Transaction(alan, 2012, 950));
		// Q1
		List<Transaction> transaction2011 = transactions.stream().filter(t -> t.getYear() == 2011)
				.sorted(comparing(Transaction::getValue)).collect(toList());
		System.out.println(transaction2011);
		List<String> techStack = Arrays.asList("JAVA", "Spring Boot", "Microservices", "Spring", "Hibernate", "Docker",
				"Junit", "JPA", "Oracle SQL");
		techStack.stream().forEach(t -> System.out.println(t));

		// Q2
		List<String> city = transactions.stream().map(t -> t.getTrader().getCity()).distinct().collect(toList());
		System.out.println(city);

		// Q3
		List<Trader> traders = transactions.stream().map(t -> t.getTrader())
				.filter(t -> "Cambridge".equals(t.getCity())).distinct().sorted(comparing(Trader::getName))
				.collect(toList());

		System.out.println(traders);

		// Q4
		String allTraders = transactions.stream().map(t -> t.getTrader().getName()).distinct().sorted().reduce("",
				(t1, t2) -> t1 + t2);

		System.out.println(allTraders);

		// Q5
		boolean milanBased = transactions.stream().anyMatch(t -> "Milan".equals(t.getTrader().getCity()));
		System.out.println(milanBased);

		// Q6
		transactions.stream().filter(t -> "Cambridge".equals(t.getTrader().getCity())).map(t -> t.getValue() + " ")
				.forEach(System.out::print);

		// Q7
		System.out.println();
		Optional<Integer> highestTransactionValue = transactions.stream().map(t -> t.getValue()).reduce(Integer::max);
		System.out.println(highestTransactionValue);

		// Q8
		Optional<Transaction> minTransaction = transactions.stream()
				.reduce((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2);

		System.out.println(minTransaction);

		// PythagoreanTriplate
		Stream<double[]> pythagoreanTriplate = IntStream.rangeClosed(1, 100).boxed()
				.flatMap(a -> IntStream.rangeClosed(a, 100)
						.mapToObj(b -> new double[] { a, b, Math.sqrt(a * a + b * b) })
						.filter(t -> t[2] % 1 == 0 && t[2] <= 100));

		pythagoreanTriplate.forEach(t -> System.out.println(t[0] + " " + t[1] + " " + t[2]));

		// iterate
		// Stream.iterate(0, n->n+2).limit(10).forEach(System.out::print);
		// fibonacci series
		Stream.iterate(new int[] { 0, 1 }, t -> new int[] { t[1], t[0] + t[1] }).limit(20)
				.forEach(t -> System.out.println(t[0]));

		// missing integer

		Integer[] arr = { 3, 4, 8, 1, 9, 66, 88, 33, 22, 0, 6, 6, 7, 7, 3, 2, 3, 2, 5 };
		List<Integer> targetList = Arrays.asList(arr);
		List<Integer> aaa = targetList.stream().sorted().filter(i -> !targetList.contains((i + 1))).collect(toList());
		System.out.println(aaa.get(0) + 1);
	}

}
