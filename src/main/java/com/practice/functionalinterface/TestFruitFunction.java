package com.practice.functionalinterface;

import java.util.ArrayList;
import java.util.List;

public class TestFruitFunction {
	
	public static void main(String[] args) {
		Fruit apple = new Fruit("apple", "red", 5);
		Fruit orange = new Fruit("orange", "yellow", 3);
		Fruit greenApple = new Fruit("green apple", "green", 8);
		Fruit guava = new Fruit("guava", "green", 2);

		List<Fruit> list = new ArrayList<>();
		list.add(apple);
		list.add(orange);
		list.add(greenApple);
		list.add(guava);
		FruitFunction.getListOfSpecifiedAttributes(list, i->i.getColor()).forEach(System.out::println);
		System.out.println();
		FruitFunction.getListOfSpecifiedAttributes(list, i->i.getName()).forEach(System.out::println);

	}


}
