package com.practice.functionalinterface;

import java.util.ArrayList;
import java.util.List;

public class FruitFunction {

	public static <T, R> List<R> getListOfSpecifiedAttributes(List<T> list, Function<T, R> func) {
		List<R> reultList = new ArrayList<>();
		for (T t : list)
			reultList.add(func.apply(t));
		return reultList;
	}

}
