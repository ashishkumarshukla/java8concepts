package com.practice.functionalinterface;

import java.util.ArrayList;
import java.util.List;

public class TestCustomFilterPredicate {

	public static void main(String[] args) {
		Fruit apple = new Fruit("apple", "red", 5);
		Fruit orange = new Fruit("orange", "yellow", 3);
		Fruit greenApple = new Fruit("green apple", "green", 8);
		Fruit guava = new Fruit("guava", "green", 2);

		List<Fruit> list = new ArrayList<>();
		list.add(apple);
		list.add(orange);
		list.add(greenApple);
		list.add(guava);

		List<Fruit> filteredList = FruitFilter.filter(list, i -> "green".equalsIgnoreCase(i.getColor()));
		System.out.println(filteredList.toString());
		List<Fruit> filteredList1 = FruitFilter.filter(list, i->i.getWegiht()>3);
		System.out.println(filteredList1.toString());

	}

}
