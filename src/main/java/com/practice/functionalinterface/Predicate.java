package com.practice.functionalinterface;

@FunctionalInterface
public interface Predicate<T> {

	boolean test(T t);

}
