package com.practice.functionalinterface;

@FunctionalInterface
public interface Function<T, R> {
	R apply(T t);
}
