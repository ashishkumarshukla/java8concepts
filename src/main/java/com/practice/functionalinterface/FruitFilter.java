package com.practice.functionalinterface;

import java.util.ArrayList;
import java.util.List;

public class FruitFilter {

	public static <T> List<T> filter(List<T> list, Predicate<T> p) {
		
		List<T> resultsList = new ArrayList<>();
		for (T s : list) {
			if (p.test(s))
				resultsList.add(s);
		}
		return resultsList;

	}

}
