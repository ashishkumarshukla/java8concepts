package com.practice.functionalinterface;

import java.util.List;

public class FruitConsumer {

	public static <T> void printSpecificAttributes(List<T> list, Consumer<T> consumer) {

		for (T t : list) {
			consumer.accept(t);
		}

	}

}
