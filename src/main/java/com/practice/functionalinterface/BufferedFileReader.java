package com.practice.functionalinterface;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferedFileReader {

	public static String processFile(BufferedReaderProcessor p) throws IOException {
		try (BufferedReader br =
			 new BufferedReader(new FileReader("D:\\poc\\java8concepts\\src\\main\\java\\com\\practice\\java\\data.txt"))) {
			return p.process(br);

		}

	}

}
