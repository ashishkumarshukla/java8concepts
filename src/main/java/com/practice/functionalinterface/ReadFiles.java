package com.practice.functionalinterface;

import java.io.IOException;

public class ReadFiles {

	public static void main(String[] args) throws IOException {
	
		String oneLine = BufferedFileReader.processFile(br -> br.readLine());
		String twoLine = BufferedFileReader.processFile(br -> br.readLine() + " " + br.readLine());
		
		System.out.println(oneLine);
		System.out.println(twoLine);
	}

}
