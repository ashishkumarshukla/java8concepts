package com.practice.functionalinterface;

@FunctionalInterface
public interface Consumer<T> {
   void accept(T t);
}
