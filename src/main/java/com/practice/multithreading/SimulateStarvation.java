package com.practice.multithreading;

public class SimulateStarvation implements Runnable {

	public void run() {
		displayThreadname();
	}

	private synchronized void displayThreadname() {
		
		System.out.println(Thread.currentThread().getName()+ " is performing some task");
		
	}

}
