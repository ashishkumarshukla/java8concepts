package com.practice.multithreading;

import java.util.List;

public class Consumer implements Runnable {
	private final List<Integer> taskQueue;

	public Consumer(List<Integer> queue) {
		this.taskQueue = queue;
	}

	@Override
	public void run() {

		while (true) {

			try {
				consume();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void consume() throws InterruptedException {
		synchronized (taskQueue) {
			while (taskQueue.isEmpty()) {
				System.out.println("Queue is empty wait for the Producer to produce some message");
				taskQueue.wait();
			}
			int i = taskQueue.remove(0);
			System.out.println("Consumes task " + i);
			taskQueue.notifyAll();

		}
	}

}
