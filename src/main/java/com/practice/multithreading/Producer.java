package com.practice.multithreading;

import java.util.List;

public class Producer implements Runnable {

	private final List<Integer> taskQueue;
	private final int capacity;

	public Producer(List<Integer> queue, int capacity) {
		this.taskQueue = queue;
		this.capacity = capacity;
	}

	@Override
	public void run() {
		int count = 0;
		while (true) {
			try {
				produce(count++);
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private void produce(int count) throws InterruptedException {
		synchronized (taskQueue) {
			while (taskQueue.size() == capacity) {
				System.out.println("Queue is full wait for the Consumer to consume some of the messages");
				taskQueue.wait();
			}

			taskQueue.add(count);
			System.out.println("Produced task " + count);
			taskQueue.notifyAll();

		}
	}

}
