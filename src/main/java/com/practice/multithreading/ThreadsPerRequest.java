package com.practice.multithreading;
public class ThreadsPerRequest {

	public static void main(String[] args) throws InterruptedException {
		for (int i = 1; i <= 10000; i++) {
			Runnable task = () -> {
				simulateDelay();
			};
			Thread t=new Thread(task);
			t.start();
		}
	}

	private static void simulateDelay() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
}
