package com.practice.multithreading;

public class DeadLock {

	public static void main(String[] args) {

		final SimulateDeadLock bObj = new SimulateDeadLock();
		for (int i = 0; i < 10; i++) {
			new Thread(() -> {
				try {
					bObj.foo();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			).start();
		}

		for (int i = 0; i < 10; i++) {
			new Thread(() -> {
				try {
					bObj.bar();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			).start();
		}
	}
}