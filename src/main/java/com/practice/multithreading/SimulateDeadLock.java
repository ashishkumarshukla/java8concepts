package com.practice.multithreading;

public class SimulateDeadLock {

	private final Object lock1 = new Object();
	private final Object lock2 = new Object();

	public void foo() throws InterruptedException {
		synchronized (lock1) {
			synchronized (lock2) {
				Thread.sleep(100);
				System.out.println("Dead");
			}
		}
	}

	public void bar() throws InterruptedException {
		synchronized (lock2) {
			synchronized (lock1) {
				Thread.sleep(100);
				System.out.println("Lock");
			}
		}

	}

}
