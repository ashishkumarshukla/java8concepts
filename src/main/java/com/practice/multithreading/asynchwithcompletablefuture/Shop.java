package com.practice.multithreading.asynchwithcompletablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class Shop {

	private String shopName;

	public Shop(String shopName) {
		this.shopName = shopName;
	}

	public String getShopName() {
		return shopName;
	}

	public double getPrice(String product) {
		return calculatePrice(product);

	}

	public Future<Double> getAsynchPriceManualImpl(String product) {
		CompletableFuture<Double> futurePrice = new CompletableFuture<>();
		new Thread(() -> {
			try {
				double price = calculatePrice(product);
				futurePrice.complete(price);
			} catch (Exception ex) {
				futurePrice.completeExceptionally(ex);
			}
		}).start();
		return futurePrice;

	}

	public Future<Double> getAsynch(String product) {
		return CompletableFuture.supplyAsync(() -> calculatePrice(product));

	}

	private double calculatePrice(String product) {
		delay();
		return Math.random() * product.charAt(0) + product.charAt(1);
	}

	private void delay() {
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
