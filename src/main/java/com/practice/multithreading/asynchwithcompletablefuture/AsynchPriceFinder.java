package com.practice.multithreading.asynchwithcompletablefuture;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import static java.util.stream.Collectors.*;

public class AsynchPriceFinder {

	private final List<Shop> shops = Arrays.asList(new Shop("BestPrice"), new Shop("LetsSaveBig"),
			new Shop("MyFavShop"), new Shop("BuyItAll"));

	private final Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100), new ThreadFactory() {
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		}
	});

	public List<String> findPrices(String product) {
		return shops.stream().map(i -> String.format("% s price is %.2f", i.getShopName(), i.getPrice(product)))
				.collect(toList());
	}

	public List<String> findPricesUsingParallelStream(String product) {
		return shops.parallelStream().map(i -> String.format("% s price is %.2f", i.getShopName(), i.getPrice(product)))
				.collect(toList());
	}

	public List<CompletableFuture<String>> findPricesUsingCompletableFuture(String product) {
		return shops.stream()
				.map(i -> CompletableFuture
						.supplyAsync(() -> String.format("% s price is %.2f", i.getShopName(), i.getPrice(product))))
				.collect(toList());
	}

	public List<String> findPricesWithCompletableFuture(String product) {
		List<CompletableFuture<String>> priceFutures = shops.stream()
				.map(i -> CompletableFuture
						.supplyAsync(() -> String.format("% s price is %.2f", i.getShopName(), i.getPrice(product))))
				.collect(toList());

		return priceFutures.stream().map(CompletableFuture::join).collect(toList());
	}
	
	public List<String> findPricesWithCompletableFutureUsingCustomExecutor(String product) {
		List<CompletableFuture<String>> priceFutures = shops.stream()
				.map(i -> CompletableFuture
						.supplyAsync(() -> String.format("% s price is %.2f", i.getShopName(), i.getPrice(product)),executor))
				.collect(toList());

		return priceFutures.stream().map(CompletableFuture::join).collect(toList());
	}

}
