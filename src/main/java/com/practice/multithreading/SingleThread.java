package com.practice.multithreading;
public class SingleThread {
	public static void main(String[] args){
		for (int i = 1; i <= 10000; i++)
			simulateDelay();
	}

	private static void simulateDelay() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

}