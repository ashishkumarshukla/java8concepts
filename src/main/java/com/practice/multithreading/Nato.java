package com.practice.multithreading;

public class Nato {

	public boolean isRansomAmountGiven() {
		return isRansomAmountGiven;
	}

	private volatile boolean isRansomAmountGiven = false;

	public void giveRansom(Terrorists terrorists) {

		while (!terrorists.isHostageReleased()) {
			System.out.println("Waiting For Hostage");
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println("Sent Moneay Waiting To Receive Hostage");
		this.isRansomAmountGiven = true;
	}

}
