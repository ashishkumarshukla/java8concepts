package com.practice.multithreading;

import java.util.ArrayList;
import java.util.List;

public class ProducerConsumerUsingWaitAndNotify {

	public static void main(String[] args) {
		List<Integer> queue = new ArrayList<Integer>();
		int capacity = 8;
		Thread producerThread = new Thread(new Producer(queue, capacity));
		Thread consumerThread = new Thread(new Consumer(queue));
		producerThread.start();
		consumerThread.start();
	}

}
