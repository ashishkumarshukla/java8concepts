package com.practice.multithreading;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MultiThreadWithExecutor {
	private static final int NBThreads = Runtime.getRuntime().availableProcessors();
	private static final Executor exec = Executors.newFixedThreadPool(NBThreads * 4 * 100);

	public static void main(String[] args) throws InterruptedException {
		for (int i = 1; i <= 10000; i++) {
			exec.execute(() -> simulateDelay());
		}
	}

	private static void simulateDelay() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

}
