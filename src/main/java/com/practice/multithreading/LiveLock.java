package com.practice.multithreading;

public class LiveLock {

	public static void main(String[] args) {
		final Terrorists terrorists = new Terrorists();
		final Nato nato = new Nato();

		new Thread(() -> terrorists.releaseHostage(nato)).start();

		new Thread(() -> nato.giveRansom(terrorists)).start();
	}

}
