package com.practice.multithreading;

public class Starvation {
	private static Runnable runnable = new SimulateStarvation();

	public static void main(String[] args) {
		for (int i=0;i<100;i++) {
		Thread thread1 = new Thread(runnable);
		thread1.setName("Thread 1");
		thread1.setPriority(10);
		Thread thread2 = new Thread(runnable);
		thread2.setName("Thread 2");
		thread2.setPriority(9);
		Thread thread3 = new Thread(runnable);
		thread3.setName("Thread 3");
		thread3.setPriority(8);
		Thread thread4 = new Thread(runnable);
		thread4.setName("Thread 4");
		thread4.setPriority(7);
		Thread thread5 = new Thread(runnable);
		thread5.setName("Thread 5");
		thread5.setPriority(6);

		
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		}
		
	}

}