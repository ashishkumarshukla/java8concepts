package com.practice.multithreading;

import java.util.concurrent.CountDownLatch;

public class Latch {

	public long timeTasks(int nbThreads, final Runnable task) throws InterruptedException {
		final CountDownLatch startGate = new CountDownLatch(1);
		final CountDownLatch endGate = new CountDownLatch(nbThreads);

		for (int i = 0; i < nbThreads; i++) {
			Thread t = new Thread() {
				public void run() {

					try {
						startGate.await();
						task.run();

					} catch (InterruptedException ex) {

					} finally {
						endGate.countDown();
					}
				}
			};
			t.start();
		}
		long start = System.nanoTime();
		startGate.countDown();
		long end = System.nanoTime();
		return end - start;
	}

}
