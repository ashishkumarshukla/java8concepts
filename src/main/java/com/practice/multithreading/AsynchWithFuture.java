package com.practice.multithreading;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AsynchWithFuture {

	private static int nThreads = Runtime.getRuntime().availableProcessors() * 400;
	private static final ExecutorService execService = Executors.newFixedThreadPool(nThreads);
	private Future<Double> future;

	public double doSomeLongComputation() throws InterruptedException {
		Thread.sleep(100);
		System.out.println("Print Name Of The  Thread");
		System.out.println(Thread.currentThread().getName());
		return 3.0;
	}

	public Future<Double> doComputation() {
		future = execService.submit(() -> doSomeLongComputation());
		return future;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		AsynchWithFuture as = new AsynchWithFuture();
		for (int i = 1; i <= 100; i++) {
		System.out.println(as.doComputation());
		System.out.println("I would be blocked by future.get()");
		}
		
	}

}
