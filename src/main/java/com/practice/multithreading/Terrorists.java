package com.practice.multithreading;

public class Terrorists {

	private volatile boolean isHostageReleased = false;

	public boolean isHostageReleased() {
		return isHostageReleased;
	}

	public void releaseHostage(Nato nato) {

		while (!nato.isRansomAmountGiven()) {
			System.out.println("Waiting For Ransom");
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Got Money-->Releasing Hostage");
		this.isHostageReleased = true;
	}

}
