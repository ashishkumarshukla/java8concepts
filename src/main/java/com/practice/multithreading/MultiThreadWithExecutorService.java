package com.practice.multithreading;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class MultiThreadWithExecutorService {
	private static int nThreads = Runtime.getRuntime().availableProcessors() * 400;
	private static final ExecutorService execService = Executors.newFixedThreadPool(nThreads);
	private static AtomicInteger count = new AtomicInteger(0);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		for (int i = 1; i <= 10000; i++) {
			execService.submit(() -> simulateDelay());
		}
		awaitTerminationAfterShutdown(execService);

	}

	public static void awaitTerminationAfterShutdown(ExecutorService execService) {
		execService.shutdown();
		try {
			if (!execService.awaitTermination(60, TimeUnit.SECONDS)) {
				execService.shutdownNow();
			}
		} catch (InterruptedException ex) {
			execService.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}

	private static void simulateDelay() {
		try {
			System.out.println(count.getAndIncrement());
			Thread.sleep(200);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
}